
const robot = require("robotjs");

function startClicking(delay) {
    let interval = setInterval(() => {
        try {
            robot.mouseClick()
        } catch (e) {
            document.write(e);
        }
    }, delay);

    return ()=> clearInterval(interval);
}

let clear = null;

module.exports = (document)=> {

    registerClick("A", 50);
    registerClick("S", 90);
    registerClick("D", 150);
    registerClick("F", 180);
    registerClick("G", 220);

    function registerClick(key, delay) {
        nw.App.registerGlobalHotKey(new nw.Shortcut({
            key : key,
            active : function() {
                if (clear == null) {
                    clear = startClicking(delay);
                } else {
                    clear();
                    clear = null;
                }
            }
        }));
    }

    nw.App.registerGlobalHotKey(new nw.Shortcut({
        key : "Alt+Tab",
        active : function() {
            if (clear != null) {
                clear();
                clear = null;
            }
        }
    }));

    document.write("Loaded ok.");
};
